package game;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;

public class Square extends JPanel {

	private static final long serialVersionUID = 1L;
	
	
	int x;
	int y;
	int width;
	int height;
	Color color = Color.white;
		
	public Square(int inX, int inY, int inWidth, int inHeight, Color inColor) {
		x = inX;
		y = inY;
		width = inWidth;
		height = inHeight;
		color = inColor;
				
	}
	
	public void changeColor(Color inColor) {
		color = inColor;
	}
	
	public void drawing() {
		repaint();
	}
	
		
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(color);
		g.fillRect(x, y, width, height);
				
	}
		
	
}
