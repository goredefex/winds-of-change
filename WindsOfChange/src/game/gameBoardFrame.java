package game;

import java.awt.*;

import javax.swing.*;

public class gameBoardFrame {
	
	private int statsWidth = 600;
	private int statsHeight = 500;
	private Room room = new Room();
	private Square[][] paintedSquareArray = new Square[MainWindsFrame.arenaRoomAmt][MainWindsFrame.arenaRoomAmt];
	private JFrame frame;
	private JPanel ArenaPanel = new JPanel();
	private ImageIcon iconPiece = new ImageIcon(gameBoardFrame.class.getResource("/game/resources/noArm.JPEG"));
	private ImageIcon doorPiece = new ImageIcon(gameBoardFrame.class.getResource("/game/resources/door.JPEG"));
	private JLabel imageCharBoardPiece = new JLabel();
	private JLabel imageDoorPiece = new JLabel();
	private TextArea infoDisplay;
	private JPanel movePanel = new JPanel();
	private List inventory;
	
	private JButton btnMoveUpward = new JButton("Move Upward");
	private JButton btnMoveRight = new JButton("Move Right");
	private JButton btnMoveLeft = new JButton("Move Left");
	private JButton btnMoveDown = new JButton("Move Down");
	private JButton btnMoveInteract = new JButton("Interact");
	private JButton btnMoveCharStat = new JButton("Stats");
	private JButton btnMoveEq = new JButton("Equip");
	private JButton btnDrop = new JButton("Drop");
	
			
	gameBoardFrame() {
		frame = new JFrame("The Winds of Change");
		frame = AdjustFrame(frame);
		
		//Arena Panel section
		ArenaPanel = AdjustPanel(ArenaPanel,MainWindsFrame.arena.getArenaColor(), MainWindsFrame.arena.getArenaPosX(),
				MainWindsFrame.arena.getArenaPosY(), MainWindsFrame.arena.getArenaWidth(), MainWindsFrame.arena.getArenaHeight());
		
		//Creation of visual room Array
		paintedSquareArray = createSquares(paintedSquareArray, room);
		paintedSquareArray = sizeAndPlaceSquares(paintedSquareArray, room);
		
		//Creation of main char board pieces
		imageCharBoardPiece = setAndPlaceMainPiece(imageCharBoardPiece, iconPiece);
		imageDoorPiece = setAndPlaceDoor(imageDoorPiece, doorPiece);
		
		//Interaction objects
		inventory = createInventory(inventory);
		infoDisplay = createInfoArea(infoDisplay);
						
		//Panel for movement buttons
		movePanel = AdjustPanel(movePanel, Color.white, MainWindsFrame.arena.getArenaPosX(), 
				MainWindsFrame.arena.getArenaHeight()+20, MainWindsFrame.arena.getArenaWidth(), 
				MainWindsFrame.arena.getArenaHeight());
		
		
		//Addition of new button "Move Up"
		btnMoveUpward = setAndPlaceButton(btnMoveUpward);
		KeyPress keyActionUp = new KeyPress(btnMoveUpward);
		btnMoveUpward = addButtonListener(keyActionUp, btnMoveUpward);
		

		//Addition of new button "Move Right"
		btnMoveRight = setAndPlaceButton(btnMoveRight);
		KeyPress keyActionRight = new KeyPress(btnMoveRight);
		btnMoveRight = addButtonListener(keyActionRight, btnMoveRight);
				
		//Addition of new button "Move Left"
		btnMoveLeft = setAndPlaceButton(btnMoveLeft);;
		KeyPress keyActionLeft = new KeyPress(btnMoveLeft);
		btnMoveLeft = addButtonListener(keyActionLeft, btnMoveLeft);
				
		//Addition of new button "Move Down"
		btnMoveDown = setAndPlaceButton(btnMoveDown);
		KeyPress keyActionDown = new KeyPress(btnMoveDown);
		btnMoveDown = addButtonListener(keyActionDown, btnMoveDown);
				
		//Addition of new button "Interact"
		btnMoveInteract = setAndPlaceButton(btnMoveInteract);
		KeyPress keyActionInt = new KeyPress(btnMoveInteract);
		btnMoveInteract = addButtonListener(keyActionInt, btnMoveInteract);
				
		//Addition of new button "Character Statistics"
		btnMoveCharStat = setAndPlaceButton(btnMoveCharStat);
		KeyPress keyActionStats = new KeyPress(btnMoveCharStat);
		btnMoveCharStat = addButtonListener(keyActionStats, btnMoveCharStat);
				
		//Addition of new button "Interact"
		btnMoveEq = setAndPlaceButton(btnMoveEq);
		KeyPress keyActionEq = new KeyPress(btnMoveEq);
		btnMoveEq = addButtonListener(keyActionEq, btnMoveEq);
				
		//Addition of new button "Drop Item"
		btnDrop = setAndPlaceButton(btnDrop);
		KeyPress keyActionDrop = new KeyPress(btnDrop);
		btnDrop = addButtonListener(keyActionDrop, btnDrop);
				
		
		//Additions to sections		
		ArenaPanel = AdditionsToPanel(ArenaPanel,imageCharBoardPiece,imageDoorPiece,paintedSquareArray);
		movePanel = AdditionsToMove(movePanel,btnMoveUpward,btnMoveRight,btnMoveLeft,btnMoveDown,btnMoveInteract,btnMoveCharStat,btnMoveEq,btnDrop);
		frame = AdditionsToFrame(frame,ArenaPanel,movePanel, inventory, infoDisplay);
				
	}
		
	private JFrame AdjustFrame(JFrame ff) { //Make JFrame
		ff.setSize(statsWidth, statsHeight);
		ff.setLocationRelativeTo(null);
		ff.setVisible(false);
		ff.setResizable(false);
		ff.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ff.getContentPane().setLayout(null);
		
		return ff;
	}
	
	private JPanel AdjustPanel(JPanel Panel, Color c, int boundX, int boundY, int boundWidth, int boundHeight) {		//Make JPanel
		Panel.setBackground(c);
		Panel.setForeground(c);
		Panel.setBounds(boundX, boundY, boundWidth, boundHeight);
		Panel.setLayout(null);
		
		return Panel;
	}
	
	private JFrame AdditionsToFrame(JFrame ff, JPanel panel, JPanel move, List inv, TextArea info) {  //Add to frame
		ff.add(panel);
		ff.add(move);						
		ff.add(inv);
		ff.add(info);
		
		return ff;
	}
	
	private JPanel AdditionsToPanel(JPanel panel, JLabel character, JLabel door, Square[][] squares) {  //Add to panel
		panel.add(character);
		panel.add(door);
		panel = addSquares(squares,panel);
		
		return panel;
	}
	
	private JPanel AdditionsToMove(JPanel move, JButton up, JButton right, JButton left, JButton down, 
			JButton inter, JButton stat, JButton eq, JButton drop) {	//Add to move panel
		move.add(up);
		move.add(right);
		move.add(left);
		move.add(down);
		
		move.add(inter);
		move.add(stat);
		move.add(eq);
		move.add(drop);
		
		return move;
	}
	
	private JPanel addSquares(Square[][] paintedSquareArray,JPanel panel) {		//add Squares to JPanel
		for(int y=0; y<5; y++) {
			for(int x=0; x<5; x++) {
				panel.add(paintedSquareArray[y][x]);
			}//end for
		}//end for
		
		return panel;
	}
	
	private JButton addButtonListener(KeyPress inKey, JButton inBtn) {
		inBtn.addActionListener(inKey);
		
		return inBtn;
	}
	
	
	private Square[][] createSquares(Square[][] paintedSquareArray, Room room) {		//Make Squares
		Color c = Color.white;
				
		for(int y=0; y<5; y++) {
			for(int x=0; x<5; x++) {
				if(y==MainWindsFrame.roomNumber[0] && x==MainWindsFrame.roomNumber[1]) {
					paintedSquareArray[y][x] = new Square(room.paintedRoomX, room.paintedRoomY, room.paintedRoomWidth, room.paintedRoomHeight, c);
				}else {
					paintedSquareArray[y][x] = new Square(room.paintedRoomX, room.paintedRoomY, room.paintedRoomWidth, room.paintedRoomHeight, room.color);
				}
			}//end for
		}//end for
				
		return paintedSquareArray;
	}
	
	private Square[][] sizeAndPlaceSquares(Square[][] paintedSquareArray, Room room) {		//Set Squares
		int startingPt = 0;
		
		for(int y=0; y<5; y++) {
			for(int x=0; x<5; x++) {
				if(x==startingPt) {
					paintedSquareArray[y][x].setBounds(startingPt, (room.boundHeight+1)*y, room.boundWidth, room.boundHeight);
				}else {
					paintedSquareArray[y][x].setBounds((room.boundWidth+1)*x, (room.boundHeight+1)*y, room.boundWidth, room.boundHeight);
				}//end if
			}//end for
		}//end for
		
		return paintedSquareArray;
	}
		
	private JButton setAndPlaceButton(JButton inBtn) {
		int[][] buttonDetails = {{80, 10, 99, 23}, //Up
								 {80, 105, 99, 23}, //Down
								 {10, 55, 99, 23}, //Left
								 {150, 55, 99, 23}, //Right
								 {10, 140, 89, 23}, //Interact
								 {10, 176, 89, 23}, //Char Stats
								 {156, 176, 89, 23}, //Equip item
								 {156, 140, 89, 23} //Drop item
								 };
		String[] btnTexts = {"Move Upward", "Move Down", "Move Left", "Move Right", "Interact", "Stats", "Equip", "Drop"};
		for(int i=0; i<btnTexts.length; i++) {
			if(inBtn.getText().equals(btnTexts[i])) {
				inBtn.setBounds(buttonDetails[i][0], buttonDetails[i][1], buttonDetails[i][2], buttonDetails[i][3]);
				break;
			}//end if
		}//end for
		
		return inBtn;
	}
	
	private JLabel setAndPlaceMainPiece(JLabel inLabel, ImageIcon img) {
		inLabel = new JLabel();
		inLabel.setIcon(img); 
		inLabel.setBounds(MainWindsFrame.roomNumber[1] * room.boundWidth+15, 
				MainWindsFrame.roomNumber[0] * room.boundHeight, 40, 45);
			
		return inLabel;
	}
	
	private JLabel setAndPlaceDoor(JLabel inLabel, ImageIcon img) {
		inLabel.setIcon(img); 
		inLabel.setVisible(false);
		inLabel.setBounds(MainWindsFrame.arena.getDoorLocation()[1] * room.boundWidth+15, 
				MainWindsFrame.arena.getDoorLocation()[0] * room.boundHeight, 40, 45);
			
		return inLabel;
	}
	
	private List createInventory(List preInv) {
		preInv = new List();
		preInv.setBounds(280, 250, 285, 210);
		preInv.add("Flash Light");
		
		return preInv;
	}
	
	private TextArea createInfoArea(TextArea info) {
		info = new TextArea();
		info.setBounds(280, 10, 285, 230);
		
		return info;
	}
	
	
	
	
	
	
	public JPanel getArenaPanel() {
		return ArenaPanel;
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public Square[][] getSquares() {
		return paintedSquareArray;
	}
	
	public Color getCurrentSquareColor() {
		return paintedSquareArray[MainWindsFrame.roomNumber[0]][MainWindsFrame.roomNumber[1]].color;
	}
	
	public void repaintSquares() {
		for(int rows=0; rows<MainWindsFrame.arenaRoomAmt; rows++) {
			for(int columns=0; columns<MainWindsFrame.arenaRoomAmt; columns++) {
					changeSquareColor(Color.black, rows, columns);
			}//end for
		}//end for	
	}
		
	public void changeSquareColor(Color c, int x, int y) {
		paintedSquareArray[x][y].changeColor(c);
	}
	
	public void doorVisible(boolean see) {
		imageDoorPiece.setVisible(see);
	}
	
	public void addToInventory(String item) {
		inventory.add(item);
	}
	
	public void addToDisplay(String item) {
		infoDisplay.append(item);
	}
	
	public void insertToDisplay(String item, int p) {
		infoDisplay.insert(item, p);
	}
	
	public Color getColorOfRoom(int x, int y) {
		return paintedSquareArray[x][y].color;
	}
	
	public int getSelectedInvIndex() {
		return inventory.getSelectedIndex();
	}
	
	public String getSelectedInvItem() {
		return inventory.getSelectedItem();
	}
	
	public void removeFrmInv(String item) {
		inventory.remove(item);
	}
	
	public void changePlayerSpot(int rm0, int rm1) {
		imageCharBoardPiece.setBounds(rm1 * room.boundWidth+15, 
				rm0 * room.boundHeight, 40, 45);
	}
	
	public void changeDoorSpot(int rm0, int rm1) {
		imageDoorPiece.setBounds(rm1 * room.boundWidth+15, 
				rm0 * room.boundHeight, 40, 45);
	}
	
	public void repaintArena() {
		ArenaPanel.repaint();
	}
	
	public Room getRoom() {
		return room;
	}
}
