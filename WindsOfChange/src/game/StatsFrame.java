package game;

import java.awt.Font;

import javax.swing.*;

public class StatsFrame {
	
	//Vars
	private int statsWidth = 350;
	private int statsHeight = 500;
	private int fSize = 18;
		
	//Containers
	private JFrame frame = new JFrame("Character Stats");;
	private JPanel statPanel = new JPanel();
	private JPanel lvlPanel = new JPanel();
	
	//Image
	private ImageIcon mainCharImg = new ImageIcon(gameBoardFrame.class.getResource("/game/resources/allArm.JPEG"));
	
	//labels
	private JLabel imageLabel = new JLabel("New label");
	private JLabel charClassLabel = new JLabel("");
	private JLabel strengthLabel = new JLabel("Strength");
	private JLabel defenseLabel = new JLabel("Defense");
	private JLabel stealthLabel = new JLabel("Stealth");
	private JLabel attackpowerLabel = new JLabel("AttackPower");
	private JLabel lvlTitleLabel = new JLabel("Inner Power");
	private JLabel levelLabel = new JLabel("Level");
	private JLabel lvlExperience = new JLabel("Experience");
	private JLabel ptsLabel = new JLabel("Points to Use");
	
	//Displayer's
	private JTextField strengthDisp = new JTextField();
	private JTextField defenseDisp  = new JTextField();
	private JTextField stealthDisp = new JTextField();
	private JTextField attackpowerDisp = new JTextField();
	private JTextField lvlDisp = new JTextField();
	private JTextField expDisp = new JTextField();
	private JTextField ptsDisp = new JTextField();
		
			
	StatsFrame() {
		
		//Containers
		frame = AdjustFrame(frame);
		
		statPanel.setName("statPanel");
		statPanel = setAndPlacePanel(statPanel);
		
		lvlPanel.setName("lvlPanel");
		lvlPanel = setAndPlacePanel(lvlPanel);
				
		//Label Section
		
		imageLabel.setName("imageLabel");
		imageLabel.setIcon(mainCharImg);
		imageLabel = setAndPlaceLabel(imageLabel);
		
		charClassLabel.setName("charClassLabel");
		charClassLabel = adjustLabel(charClassLabel, fSize+15);
		charClassLabel = setAndPlaceLabel(charClassLabel);
				
		strengthLabel.setName("strengthLabel");
		strengthLabel = adjustLabel(strengthLabel, fSize);
		strengthLabel = setAndPlaceLabel(strengthLabel);
		
		defenseLabel.setName("defenseLabel");
		defenseLabel = adjustLabel(defenseLabel, fSize);
		defenseLabel = setAndPlaceLabel(defenseLabel);
				
		stealthLabel.setName("stealthLabel");
		stealthLabel = adjustLabel(stealthLabel, fSize);
		stealthLabel = setAndPlaceLabel(stealthLabel);
				
		attackpowerLabel.setName("attackpowerLabel");
		attackpowerLabel = adjustLabel(attackpowerLabel, fSize);
		attackpowerLabel = setAndPlaceLabel(attackpowerLabel);
		
		lvlTitleLabel.setName("lvlTitleLabel");
		lvlTitleLabel = adjustLabel(lvlTitleLabel, fSize+15);
		lvlTitleLabel = setAndPlaceLabel(lvlTitleLabel);

		levelLabel.setName("levelLabel");
		levelLabel = adjustLabel(levelLabel, fSize);
		levelLabel = setAndPlaceLabel(levelLabel);
		
		lvlExperience.setName("lvlExperience");
		lvlExperience = adjustLabel(lvlExperience, fSize);
		lvlExperience = setAndPlaceLabel(lvlExperience);
		
		ptsLabel.setName("ptsLabel");
		ptsLabel = adjustLabel(ptsLabel, fSize);
		ptsLabel = setAndPlaceLabel(ptsLabel);
				
		
		//Displayer Section
		strengthDisp.setName("strengthDisp");
		strengthDisp = setAndPlaceDisplayers(strengthDisp);
		strengthDisp = adjustDisplayers(strengthDisp);
		
		defenseDisp.setName("defenseDisp");
		defenseDisp = setAndPlaceDisplayers(defenseDisp);
		defenseDisp = adjustDisplayers(defenseDisp);
		
		stealthDisp.setName("stealthDisp");
		stealthDisp = setAndPlaceDisplayers(stealthDisp);
		stealthDisp = adjustDisplayers(stealthDisp);
		
		attackpowerDisp.setName("attackpowerDisp");
		attackpowerDisp = setAndPlaceDisplayers(attackpowerDisp);
		attackpowerDisp = adjustDisplayers(attackpowerDisp);
				
		lvlDisp.setName("lvlDisp");
		lvlDisp = setAndPlaceDisplayers(lvlDisp);
		lvlDisp = adjustDisplayers(lvlDisp);
		
		expDisp.setName("expDisp");
		expDisp = setAndPlaceDisplayers(expDisp);
		expDisp = adjustDisplayers(expDisp);
		
		ptsDisp.setName("ptsDisp");
		ptsDisp = setAndPlaceDisplayers(ptsDisp);
		ptsDisp = adjustDisplayers(ptsDisp);
		
		//Additions
		statPanel = additionsToStatPanel(statPanel,imageLabel,charClassLabel,strengthLabel,defenseLabel,
				stealthLabel,attackpowerLabel,strengthDisp,defenseDisp,stealthDisp,attackpowerDisp);
		lvlPanel = additionsToLvlPanel(lvlPanel,lvlTitleLabel,levelLabel,lvlExperience,ptsLabel,
				lvlDisp,expDisp,ptsDisp);
		frame = additionsToFrame(frame,statPanel,lvlPanel);
		
		//Populate Initial Stat Fields
		
		
	} //end constructor
		
	private JFrame AdjustFrame(JFrame ff) { //Make JFrame
		ff.setSize(statsWidth, statsHeight);
		ff.setLocationRelativeTo(null);
		ff.setVisible(false);
		ff.setResizable(false);
		ff.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		ff.getContentPane().setLayout(null);
		
		return ff;
	}
		
	private JLabel setAndPlaceLabel(JLabel jj) {
		int[][] labelBounds = {{34, 11, 256, 59}, //title
							   {0, 81, 99, 34}, //strength
							   {0, 115, 99, 34}, //defense
							   {0, 160, 99, 34}, //stealth
							   {0, 205, 134, 34}, //attack
							   {205, 74, 99, 173}, //image
							   {31, 0, 256, 59}, //lvl title
							   {0, 48, 130, 34}, //level
							   {0, 93, 130, 34}, //exp
							   {0, 138, 130, 34}, //pts
							  }; 
							   
		String labelNames[] = {"charClassLabel","strengthLabel","defenseLabel","stealthLabel",
								"attackpowerLabel","imageLabel","lvlTitleLabel","levelLabel",
								"lvlExperience","ptsLabel"};
		
		for(int i=0; i<labelNames.length; i++) {
			if(jj.getName().equals(labelNames[i])) {
				jj.setBounds(labelBounds[i][0], labelBounds[i][1], labelBounds[i][2], labelBounds[i][3]);
				break;
			}//End if
		}//End for
				
		return jj;
	}
	
	private JLabel adjustLabel(JLabel jj, int size) {
		jj.setHorizontalAlignment(SwingConstants.CENTER);
		jj.setFont(new Font("Tahoma", Font.BOLD, size));
						
		return jj;
	}
	
	private JPanel setAndPlacePanel(JPanel panel) {
		int[][] panelBounds = {{10, 11, 314, 258}, //stat
							   {10, 280, 314, 171} //lvl
							  }; 
							   
		String panelNames[] = {"statPanel","lvlPanel"};
		
		for(int i=0; i<panelNames.length; i++) {
			if(panel.getName().equals(panelNames[i])) {
				panel.setBounds(panelBounds[i][0], panelBounds[i][1], panelBounds[i][2], panelBounds[i][3]);
				break;
			}//End if
		}//End for
		panel.setLayout(null);
		
		return panel;
	}
	
	private JTextField setAndPlaceDisplayers(JTextField jF) {
		int[][] fieldBounds = {{145, 79, 50, 20}, //strength
				   			   {145, 125, 50, 20}, //defense
				   			   {145, 170, 50, 20}, //stealth
				   			   {144, 215, 50, 20}, //attack
				   			   {184, 58, 50, 20}, //lvl
   				   			   {184, 103, 50, 20}, //exp
				   			   {184, 148, 50, 20}, //pts
				   			  }; 
				   
		String fieldNames[] = {"strengthDisp","defenseDisp","stealthDisp","attackpowerDisp","lvlDisp","expDisp","ptsDisp"};
		
		for(int i=0; i<fieldNames.length; i++) {
			if(jF.getName().equals(fieldNames[i])) {
				jF.setBounds(fieldBounds[i][0], fieldBounds[i][1], fieldBounds[i][2], fieldBounds[i][3]);
				break;
			}//End if
		}//End for
		
		
		
		return jF;
	}
	
	private JTextField adjustDisplayers(JTextField jF) {
		jF.setEditable(false);
		jF.setColumns(10);	
			
			return jF;
	}
	
	private JPanel additionsToStatPanel(JPanel panel,JLabel img,JLabel title,JLabel str,JLabel def,
			JLabel stlth,JLabel attk,JTextField strDisp,JTextField defDisp,JTextField stlthDisp,
			JTextField attkDisp) {
		panel.add(img);
		panel.add(title);
		panel.add(str);
		panel.add(def);
		panel.add(stlth);
		panel.add(attk);
		panel.add(strDisp);
		panel.add(defDisp);
		panel.add(stlthDisp);
		panel.add(attkDisp);
		
		return panel;
	}

	private JPanel additionsToLvlPanel(JPanel panel,JLabel title,JLabel lvl,JLabel exp,JLabel pts,
			JTextField lvlDisp,JTextField expDisp,JTextField ptsDisp) {
		panel.add(title);
		panel.add(lvl);
		panel.add(exp);
		panel.add(pts);
		panel.add(lvlDisp);
		panel.add(expDisp);
		panel.add(ptsDisp);
		
		return panel;
	}

	private JFrame additionsToFrame(JFrame framed,JPanel statP,JPanel lvlP) {
		framed.add(statP);
		framed.add(lvlP);
		
		return framed;
	}
	
	
	
	
	
	
	public JLabel getCharClassLabel() {
		return charClassLabel;
	}
		
	public JFrame getFrame() {
		return frame;
	}
	
	public JTextField getStrDisp() {
		return strengthDisp;
	}
	
	public void setCharTitle() {
		if(MainWindsFrame.elite==true) {
			getCharClassLabel().setText("Elite");
		}else if(MainWindsFrame.assasin==true) {
			charClassLabel.setText("Assasin");
		}else if(MainWindsFrame.guardian==true) {
			charClassLabel.setText("Guardian");
		}
	}
	
	public void setStrengthDisp(int number) {
		strengthDisp.setText(String.valueOf(number));
	}
	
	public int getCurrentStr() {
		int num = 0;
		if(MainWindsFrame.elite==true) {
			num = MainWindsFrame.eliteChar.getStr();
		}else if(MainWindsFrame.assasin==true) {
			num = MainWindsFrame.assasinChar.getStr();
		}if(MainWindsFrame.guardian==true) {
			num = MainWindsFrame.guardianChar.getStr();
		}
				
		return num;
	}
	
	public void setDefDisp(int number) {
		defenseDisp.setText(String.valueOf(number));
	}
	
	public int getCurrentDef() {
		int num = 0;
		if(MainWindsFrame.elite==true) {
			num = MainWindsFrame.eliteChar.getDef();
		}else if(MainWindsFrame.assasin==true) {
			num = MainWindsFrame.assasinChar.getDef();
		}if(MainWindsFrame.guardian==true) {
			num = MainWindsFrame.guardianChar.getDef();
		}
				
		return num;
	}
	

	public void setStealthDisp(int number) {
		stealthDisp.setText(String.valueOf(number));
	}
	
	public int getCurrentStealth() {
		int num = 0;
		if(MainWindsFrame.elite==true) {
			num = MainWindsFrame.eliteChar.getStealth();
		}else if(MainWindsFrame.assasin==true) {
			num = MainWindsFrame.assasinChar.getStealth();
		}if(MainWindsFrame.guardian==true) {
			num = MainWindsFrame.guardianChar.getStealth();
		}
				
		return num;
	}
	
	public void setAttackDisp(int number) {
		attackpowerDisp.setText(String.valueOf(number));
	}
	
	public int getCurrentAttack() {
		int num = 0;
		if(MainWindsFrame.elite==true) {
			num = MainWindsFrame.eliteChar.getAtk();
		}else if(MainWindsFrame.assasin==true) {
			num = MainWindsFrame.assasinChar.getAtk();
		}if(MainWindsFrame.guardian==true) {
			num = MainWindsFrame.guardianChar.getAtk();
		}
				
		return num;
	}
	
	public void setLvlDisp(int number) {
		lvlDisp.setText(String.valueOf(number));
	}
	
	public int getCurrentLvl() {
		int num = 0;
		if(MainWindsFrame.elite==true) {
			num = MainWindsFrame.eliteChar.getLvl();
		}else if(MainWindsFrame.assasin==true) {
			num = MainWindsFrame.assasinChar.getLvl();
		}if(MainWindsFrame.guardian==true) {
			num = MainWindsFrame.guardianChar.getLvl();
		}
				
		return num;
	}
	
	public void setEXPDisp(int number) {
		expDisp.setText(String.valueOf(number));
	}
	
	public int getCurrentEXP() {
		int num = 0;
		if(MainWindsFrame.elite==true) {
			num = MainWindsFrame.eliteChar.getEXP();
		}else if(MainWindsFrame.assasin==true) {
			num = MainWindsFrame.assasinChar.getEXP();
		}if(MainWindsFrame.guardian==true) {
			num = MainWindsFrame.guardianChar.getEXP();
		}
				
		return num;
	}
	
	public void setPtsDisp(int number) {
		ptsDisp.setText(String.valueOf(number));
	}
	
	public int getCurrentPts() {
		int num = 0;
		if(MainWindsFrame.elite==true) {
			num = MainWindsFrame.eliteChar.getLvlPts();
		}else if(MainWindsFrame.assasin==true) {
			num = MainWindsFrame.assasinChar.getLvlPts();
		}if(MainWindsFrame.guardian==true) {
			num = MainWindsFrame.guardianChar.getLvlPts();
		}
				
		return num;
	}
	
}
