package game;

/*
 * 
 * Authored By: Christian Bowyer, Feb 18th, 2013*/

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.Timer.*;
import java.util.Timer;


public class MainWindsFrame {
	
	//Static player name
	public String playerName;
	
	//Static timer begins
	//public static TimerStorage t = new TimerStorage();
	
	//Static game texts
	public static TextFiles texts = new TextFiles();
	
	//Static Menu Variables
	public static StartMenu menuMain = new StartMenu();
	public static JFrame OMenu = menuMain.getFrame();
	public static boolean guardian, elite, assasin;
	
	//Static Character Classes
	public static Elite eliteChar;
	public static Guardian guardianChar;
	public static Assasin assasinChar;
	
	//Static Game Variables
	public static int arenaRoomAmt = 5;
	public static int roomNumber[] = {generateRandRoom(),generateRandRoom()};
	public static Arena arena = new Arena();
	public static gameBoardFrame board = new gameBoardFrame();
	public static StatsFrame stats = new StatsFrame();
	public static JFrame ff = board.getFrame();
	public static JFrame characterStats = stats.getFrame();
	//public static GameItems items;
		
	//Static methods
	public static int generateRandRoom() {
		Random r = new Random();
		int rand = r.nextInt(4);
		return rand;
	}

	public static void resetCurrentRoom() {
		roomNumber[0] = generateRandRoom();
		roomNumber[1] = generateRandRoom();
	}
	
	public static int[] getRoomNumber() {
		return roomNumber;
	}
		
	public static void setChosenType(String c) {
		if(c.equals("GUARDIAN")) {
			guardian = true;
		}else if(c.equals("ELITE")) {
			elite = true;
		}else if(c.equals("ASSASIN")) {
			assasin = true;
		}
		
		setCharType();
	}
	
	private static void setCharType() {
		if(elite==true) {
			eliteChar = new Elite("elite");
		}else if(guardian==true) {
			guardianChar = new Guardian("guardian");
		}else if(assasin==true) {
			assasinChar = new Assasin("assasin");
		} //end if
		
	}
		
	//   --------------------------------------------------End Static
		
	
	//---------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------
	
	
		
		
	
	//Main Method of game run time
	public void start() {
		
		//All game post-declarations stored here
		arena = updateArenaRoomArray(roomNumber, arena);
		
		
		
	}	  //-----*****END OF START ******------  
	
	
	
	//Start of methods
	public Arena updateArenaRoomArray(int[] roomNumber, Arena a) {		//Feed roomNumber to Arena and set seat switch
		a.setUncoveredRoom(roomNumber);
				
		return arena;
	}
	
	public void setPlayerName(String n) {
		playerName = n;
	}
	
	public String getPlayerName() {
		return playerName;
	}
		
	
}//EOC