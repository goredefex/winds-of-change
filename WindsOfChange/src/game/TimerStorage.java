package game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;


public class TimerStorage extends TimerTask {

	private final long DELAY = 0;
	private final long tLength = 50;
	
	private int eventCounter = 0;
	private Timer eventChanceTimer;
	
		
	TimerStorage() {
		eventChanceTimer = new Timer();
		eventChanceTimer.scheduleAtFixedRate(this, DELAY, tLength);
		
	}
	
	public Timer getEventTimer() {
		return eventChanceTimer;
	}
	
	public int getCounter() {
		return eventCounter;
	}
	
	
	@Override
	public void run() {
		eventCounter++;
		
		if(eventCounter==100) {
			eventCounter = 0;
		}//End if
			
		
	}
	
}
