package game;

import java.awt.Color;
import java.util.*;

import javax.swing.JOptionPane;

public class Arena {
	
	private static TimerStorage t = new TimerStorage();
	
	private int cubeSize = 5;
	private boolean[][] arena = new boolean[cubeSize][cubeSize];
	private int startChanceRange = 0;
	private int finalChanceRange = 100;
	
	//odds are 1 in (n) chances
	private int battleChance = 5;
	private int itemChance = 5;
				
	private final int arenaWidth = 255;
	private final int arenaHeight = 230;
	private final int arenaPosX = 10;
	private final int arenaPosY = 10;
	private final Color color = Color.black;
		
	private int[] keyLocation = {generateRand(cubeSize-1), generateRand(cubeSize-1)};
	private int[] doorLocation = {generateRand(cubeSize-1), generateRand(cubeSize-1)};
	
	private int turn = 1;
	private int level = 1;		
	
	Battle battle;
	GameItems items = new GameItems();
	
	//default constructor
	Arena() {
		keyLocation = validateAlike(keyLocation, doorLocation);
								
	}
	
	//private methods
	private int generateRand(int in) {
		Random r = new Random();
		int rand = r.nextInt(in);
		return rand;
	}
	
	private int[] validateAlike(int[] key, int[] door) {
		if(key[0]==door[0] && key[1]==door[1]) {
			if(key[0]==0) {
				key[0]++;
			}else if(key[0]==4) {
				key[0]--;
			}//end if
			
		}//end if
		
		return keyLocation;
	}
	
	private int calcChances(int givenOdds) {
		int calc = finalChanceRange - startChanceRange;
		calc /= givenOdds;
		
		return calc;
	}
	
	
	
	
	
	
	
	//public methods
	public void newArena() {
		keyLocation[0] = generateRand(cubeSize-1);
		keyLocation[1] = generateRand(cubeSize-1);
		doorLocation[0] = generateRand(cubeSize-1);
		doorLocation[1] = generateRand(cubeSize-1);
		keyLocation = validateAlike(keyLocation, doorLocation);
		turn = 1;
		resetBoolArray(arena);		
	}
	
	public void resetBoolArray(boolean[][] array) {
		for(int rows=0; rows==cubeSize-1; rows++) {
			for(int columns=0; columns==cubeSize-1; columns++) {
				array[rows][columns] = false;
			}	
		}
	}
	
	public int[] getDoorLocation() {
		return doorLocation;
	}
	
	public int[] getKeyLocation() {
		return keyLocation;
	}
	
	public int getArenaWidth() {
		return arenaWidth;
	}
	
	public int getArenaHeight() {
		return arenaHeight;
	}
	
	public int getArenaPosX() {
		return arenaPosX;
	}
	
	public int getArenaPosY() {
		return arenaPosY;
	}
	
	public Color getArenaColor() {
		return color;
	}
	
	public int getCurrentLvl() {
		return level;
	}
	
	public void floorLvlUp() {
		level++;
	}
	
	public void nextTurn() {
		turn++;
	}
	
	public void resetTurnCounter() {
		turn = 1;
	}
		
	public void setUncoveredRoom(int[] roomNumber) {
		Square[][] sq = MainWindsFrame.board.getSquares();
		
		if(MainWindsFrame.board.getCurrentSquareColor()==Color.black || turn == 1) {
			arena[roomNumber[0]][roomNumber[1]] = true;
			checkForEventInRoom(roomNumber);
			MainWindsFrame.board.changeSquareColor(Color.white, roomNumber[0], roomNumber[1]);
			
			if(roomNumber[0]==keyLocation[0] && roomNumber[1]==keyLocation[1]) {
				MainWindsFrame.board.addToInventory("Lvl " + level + " Key");
				MainWindsFrame.board.addToDisplay("\nYou found a key in level " + level + "!");
			
			}else if(roomNumber[0]==doorLocation[0] && roomNumber[1]==doorLocation[1]) {
				
				if(turn==1) {
					if(doorLocation[1]>=0 && doorLocation[1]<4) {
						doorLocation[1]++;
						MainWindsFrame.board.changeDoorSpot(roomNumber[0],roomNumber[1]);
					}else if(doorLocation[1]==4) {
						doorLocation[1]--;
						MainWindsFrame.board.changeDoorSpot(roomNumber[0],roomNumber[1]);
					}//end door if
				}else {
					MainWindsFrame.board.doorVisible(true);
					MainWindsFrame.board.addToDisplay("\nYou found a door in level" + level + "!");
				
				}//end turn if
			}//end key/door uncover if
			
			
		}//end black square if
		

			
	}//end method uncovered room
	
	public void checkForEventInRoom(int[] inRoom) {
		if(turn>1) {
				int battleSpecificOdds = calcChances(battleChance);
				int itemSpecificOdds = battleSpecificOdds + calcChances(itemChance);
				if(t.getCounter()>=startChanceRange && t.getCounter()<=battleSpecificOdds) {
					battle = new Battle();
				}else if(t.getCounter()>battleSpecificOdds && t.getCounter()<=itemSpecificOdds) {
					MainWindsFrame.board.addToInventory(items.getRandItem());
				}//end chance if
			}//end turn if
	}//end method
		
			
}

