package game;

public class Guardian extends NewCharacter {
	
	private String className;
	
	//Specific char stats
	private int strG = 5;
	private int defG = 10;
	private int HPG = 500;
	private int stealthG = 1;
	private int attackG = 100;
	
	Guardian(String nn) {
		super(nn);
		className = nn;
		this.addStr(strG);
		this.addDef(defG);
		this.addHP(HPG);
		this.addStealth(stealthG);
		this.addAtk(attackG);
		this.setCurrentGameHp(this.getHP());
		
	}
	
}


