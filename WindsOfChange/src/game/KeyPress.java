package game;

import java.awt.Color;
import java.awt.event.*;
import javax.swing.*;

public class KeyPress implements ActionListener {

	public int[] safety = new int[2];
	private boolean[] buttonPushed = new boolean[8];
	private boolean moveButton, interact, stats, equip, drop;
	
	//Constructor built with current button that was triggered
	KeyPress(JButton inBtn) {
		String[] btnTexts = {"Move Upward", "Move Down", "Move Left", "Move Right", "Interact", "Stats", "Equip", "Drop"};
		outerloop:
		for(int i=0; i<btnTexts.length; i++) {
			if(inBtn.getText().equals(btnTexts[i])) {
				buttonPushed[i]=true;
				break outerloop;
			}//end if
		}//end for
		moveButton = isAMoveButton(moveButton);
		interact = interaction(interact);
		drop = dropButton(drop);
		stats = statsButton(stats);		
		
	}
	
	public boolean interaction(boolean answer) {
		if(buttonPushed[4]==true) {
			answer = true;
		}
		return answer;
	}
	
	public boolean dropButton(boolean answer) {
		if(buttonPushed[7]==true) {
			answer = true;
		}
		return answer;
	}
	
	public boolean statsButton(boolean answer) {
		if(buttonPushed[5]==true) {
			answer = true;
		}
		return answer;
	}
	
	public boolean isAMoveButton(boolean answer) {
		for(int i=0; i<4; i++) {
			if(buttonPushed[i]==true) {
				answer = true;
			}
		}
		return answer;
	}
	
	//Change room to new room
	public void moveToRoom() {
		updateSafety();
		
		if(buttonPushed[0]==true) {
			MainWindsFrame.roomNumber[0]--;
		}else if(buttonPushed[1]==true) {
			MainWindsFrame.roomNumber[0]++;
		}else if(buttonPushed[2]==true) {
			MainWindsFrame.roomNumber[1]--;
		}else if(buttonPushed[3]==true) {
			MainWindsFrame.roomNumber[1]++;
		}//end if
		
	}
	
	
	public void updateSafety() {
		safety[0] = MainWindsFrame.roomNumber[0];
		safety[1] = MainWindsFrame.roomNumber[1];
	}
	
	public void useSafety() {
		MainWindsFrame.roomNumber[0] = safety[0];
		MainWindsFrame.roomNumber[1] = safety[1];
		MainWindsFrame.board.addToDisplay("\nYou cant go through walls");
	}
	
	private void lightsOff() {
		int[] rm = MainWindsFrame.getRoomNumber();
		MainWindsFrame.board.changeSquareColor(Color.black, rm[0], rm[1]);
	}
	
	private void lightsON() {
		int[] rm = MainWindsFrame.getRoomNumber();
		MainWindsFrame.board.changeSquareColor(Color.white, rm[0], rm[1]);
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		int[] currentRm = MainWindsFrame.getRoomNumber();
		int[] currentDoor = MainWindsFrame.arena.getDoorLocation();
		int[] currentKey = MainWindsFrame.arena.getKeyLocation();
		String lvl = String.valueOf(MainWindsFrame.arena.getCurrentLvl());
		
		if(interact==true) {       //Interact Button
			int isSelected = MainWindsFrame.board.getSelectedInvIndex();
			//everything that isnt flashlight
			if(isSelected>0) {
				if(MainWindsFrame.board.getSelectedInvItem().equals("Lvl " + lvl + " Key")) {
					if(currentRm[0]==currentDoor[0] && currentRm[1]==currentDoor[1]) {
						MainWindsFrame.arena.floorLvlUp();
						MainWindsFrame.arena.newArena();
						int[] newDoorSpot = MainWindsFrame.arena.getDoorLocation();
						MainWindsFrame.board.repaintSquares();
						MainWindsFrame.resetCurrentRoom();
						MainWindsFrame.board.changePlayerSpot(currentRm[0], currentRm[1]);
						MainWindsFrame.board.changeDoorSpot(newDoorSpot[0], newDoorSpot[1]);
						MainWindsFrame.board.doorVisible(false);
						MainWindsFrame.arena.setUncoveredRoom(MainWindsFrame.getRoomNumber());
						MainWindsFrame.board.repaintArena();
						MainWindsFrame.board.removeFrmInv("Lvl " + lvl + " Key");
					}else {
						MainWindsFrame.board.addToDisplay("\nThis has no use in this room");
					}//end if	
				}//end if
			//flashlight related event	
			}else if(isSelected==0) {
				if(MainWindsFrame.board.getColorOfRoom(currentRm[0],currentRm[1])==Color.black) {
					lightsON();
					MainWindsFrame.board.repaintArena();
				}else if(MainWindsFrame.board.getColorOfRoom(currentRm[0],currentRm[1])==Color.white) {
					lightsOff();
					MainWindsFrame.board.repaintArena();
				}//end if
			//hasnt chosen any item to interact with	
			}else if(isSelected<0) {
				MainWindsFrame.board.addToDisplay("\nPlease choose and item before interacting with it.");
			}//end if	
		
		}else if(drop==true) {		//Drop button
			int selection = MainWindsFrame.board.getSelectedInvIndex();
			if(selection==0) {
				MainWindsFrame.board.addToDisplay("\nIts too dangerous to drop your flashlight");
			}else if(selection>0) {
				if(MainWindsFrame.board.getSelectedInvItem().equals("Lvl " + lvl + " Key")) {
					MainWindsFrame.board.addToDisplay("\nDropping this key will ensure being trapped.");
				}else {
					MainWindsFrame.board.removeFrmInv(MainWindsFrame.board.getSelectedInvItem());
				}//end if
				
			}else if(selection==-1) {
				MainWindsFrame.board.addToDisplay("\nPlease choose and item before dropping it.");
			}//end if
		
		}else if(stats==true) {		//Stats button
			MainWindsFrame.stats.setCharTitle();
			MainWindsFrame.stats.setStrengthDisp(MainWindsFrame.stats.getCurrentStr());
			MainWindsFrame.stats.setDefDisp(MainWindsFrame.stats.getCurrentDef());
			MainWindsFrame.stats.setStealthDisp(MainWindsFrame.stats.getCurrentStealth());
			MainWindsFrame.stats.setAttackDisp(MainWindsFrame.stats.getCurrentAttack());
			MainWindsFrame.stats.setLvlDisp(MainWindsFrame.stats.getCurrentLvl());
			MainWindsFrame.stats.setEXPDisp(MainWindsFrame.stats.getCurrentEXP());
			MainWindsFrame.stats.setPtsDisp(MainWindsFrame.stats.getCurrentPts());
			MainWindsFrame.characterStats.setVisible(true);
		
		}else if(equip==true) {
			
			//
			
		}else if(moveButton==true) {		//Move button
			moveToRoom();
			if(currentRm[0]<0 || currentRm[1]<0 || currentRm[0]>4 || currentRm[1]>4) {
				useSafety();	
			}else {
				MainWindsFrame.arena.setUncoveredRoom(currentRm);
				//MainWindsFrame.board.changeSquareColor(Color.white, currentRm[0], currentRm[1]);
				MainWindsFrame.board.changePlayerSpot(currentRm[0], currentRm[1]);
				MainWindsFrame.board.repaintArena();
				MainWindsFrame.arena.nextTurn();
			}//end if		
		}//end if						
	}	
	
	

}
