package game;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;
import java.awt.List;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.TextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;

public class test {

	private JFrame frame;
	private JTextField strengthDisp;
	private JTextField defenseDisp;
	private JTextField stealthDisp;
	private JTextField attackpowerDisp;
	private JTextField lvlDisp;
	private JTextField expDisp;
	private JTextField ptsDisp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					test window = new test();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public test() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 350, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel charClassLabel = new JLabel("Assasin");
		charClassLabel.setHorizontalAlignment(SwingConstants.CENTER);
		charClassLabel.setFont(new Font("Tahoma", Font.BOLD, 33));
		charClassLabel.setBounds(34, 11, 256, 59);
		frame.getContentPane().add(charClassLabel);
		
		JLabel strengthLabel = new JLabel("Strength");
		strengthLabel.setHorizontalAlignment(SwingConstants.CENTER);
		strengthLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		strengthLabel.setBounds(10, 81, 99, 34);
		frame.getContentPane().add(strengthLabel);
		
		JPanel statPanel = new JPanel();
		statPanel.setBounds(10, 11, 314, 258);
		frame.getContentPane().add(statPanel);
		statPanel.setLayout(null);
		
		JLabel defenseLabel = new JLabel("Defense");
		defenseLabel.setHorizontalAlignment(SwingConstants.CENTER);
		defenseLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		defenseLabel.setBounds(0, 115, 99, 34);
		statPanel.add(defenseLabel);
		
		JLabel stealthLabel = new JLabel("Stealth");
		stealthLabel.setHorizontalAlignment(SwingConstants.CENTER);
		stealthLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		stealthLabel.setBounds(0, 160, 99, 34);
		statPanel.add(stealthLabel);
		
		JLabel attackpowerLabel = new JLabel("AttackPower");
		attackpowerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		attackpowerLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		attackpowerLabel.setBounds(0, 205, 134, 34);
		statPanel.add(attackpowerLabel);
		
		strengthDisp = new JTextField();
		strengthDisp.setEditable(false);
		strengthDisp.setBounds(145, 79, 50, 20);
		statPanel.add(strengthDisp);
		strengthDisp.setColumns(10);
		
		defenseDisp = new JTextField();
		defenseDisp.setEditable(false);
		defenseDisp.setColumns(10);
		defenseDisp.setBounds(145, 125, 50, 20);
		statPanel.add(defenseDisp);
		
		stealthDisp = new JTextField();
		stealthDisp.setEditable(false);
		stealthDisp.setColumns(10);
		stealthDisp.setBounds(145, 170, 50, 20);
		statPanel.add(stealthDisp);
		
		attackpowerDisp = new JTextField();
		attackpowerDisp.setEditable(false);
		attackpowerDisp.setColumns(10);
		attackpowerDisp.setBounds(144, 215, 50, 20);
		statPanel.add(attackpowerDisp);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(205, 74, 99, 173);
		statPanel.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon(test.class.getResource("/game/resources/allArm.jpeg")));
		
		JPanel lvlPanel = new JPanel();
		lvlPanel.setBounds(10, 280, 314, 171);
		frame.getContentPane().add(lvlPanel);
		lvlPanel.setLayout(null);
		
		JLabel lvlLabel = new JLabel("Inner Power");
		lvlLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lvlLabel.setFont(new Font("Tahoma", Font.BOLD, 33));
		lvlLabel.setBounds(31, 0, 256, 59);
		lvlPanel.add(lvlLabel);
		
		JLabel levelLabel = new JLabel("Level");
		levelLabel.setHorizontalAlignment(SwingConstants.CENTER);
		levelLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		levelLabel.setBounds(0, 48, 130, 34);
		lvlPanel.add(levelLabel);
		
		JLabel lblExperience = new JLabel("Experience");
		lblExperience.setHorizontalAlignment(SwingConstants.CENTER);
		lblExperience.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblExperience.setBounds(0, 93, 130, 34);
		lvlPanel.add(lblExperience);
		
		JLabel ptsLabel = new JLabel("Points to Use");
		ptsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		ptsLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		ptsLabel.setBounds(0, 138, 130, 34);
		lvlPanel.add(ptsLabel);
		
		lvlDisp = new JTextField();
		lvlDisp.setEditable(false);
		lvlDisp.setColumns(10);
		lvlDisp.setBounds(184, 58, 50, 20);
		lvlPanel.add(lvlDisp);
		
		expDisp = new JTextField();
		expDisp.setEditable(false);
		expDisp.setColumns(10);
		expDisp.setBounds(184, 103, 50, 20);
		lvlPanel.add(expDisp);
		
		ptsDisp = new JTextField();
		ptsDisp.setEditable(false);
		ptsDisp.setColumns(10);
		ptsDisp.setBounds(184, 148, 50, 20);
		lvlPanel.add(ptsDisp);
		
		
	}
}
