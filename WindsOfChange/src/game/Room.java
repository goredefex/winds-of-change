package game;

import java.awt.Color;


public class Room {

	public final int paintedRoomX = 1;
	public final int paintedRoomY = 1;
	public final int paintedRoomWidth = 250;
	public final int paintedRoomHeight = 250;
	public final int boundWidth = 50;
	public final int boundHeight = 45;
		
	public Color color = Color.black;
		
	public void changeColor(Color inColor) {
		color = inColor;
	}
	
}
