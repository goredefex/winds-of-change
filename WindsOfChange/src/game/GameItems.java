package game;

import java.util.Random;

public class GameItems {

	private String[] possibleItem = {"Large Potion","Small Potion","Strength Stone","Defense Stone","Stealth Stone","Attack Stone"};
	
	private int lgPot;
	private int smPot = 250;
	private int strStone = 5;
	private int defStone = 5;
	private int stlthStone = 5;
	private int atkStone = 5;
		
	
	//default constructor
	GameItems() {
		
	}
	
	private int generateRand() {
		Random r = new Random();
		int rand = r.nextInt(5);
		return rand;
	}
	
	
	
	
	public String getSpecificItem(int num) {
		return possibleItem[num];
	}
	
	public String getRandItem() {
		return possibleItem[generateRand()];
	}
	
	public int getLgPotStat() {
		int missingHp = 0;
		if(MainWindsFrame.elite==true) {
			missingHp = MainWindsFrame.eliteChar.getHP();
			missingHp -= MainWindsFrame.eliteChar.getOnGoingGameHp();
		}else if(MainWindsFrame.assasin==true) {
			missingHp = MainWindsFrame.assasinChar.getHP();
			missingHp -= MainWindsFrame.assasinChar.getOnGoingGameHp();
		}else if(MainWindsFrame.guardian==true) {
			missingHp = MainWindsFrame.guardianChar.getHP();
			missingHp -= MainWindsFrame.guardianChar.getOnGoingGameHp();
		}
		
		
		return missingHp;
	}
	
	public int getSmPotStat() {
		return smPot;
	}
	
	public int getStrStone() {
		return strStone;
	}
	
	public int getDefStone() {
		return defStone;
	}
	
	public int getStealthStone() {
		return stlthStone;
	}
	
	public int getAttackStone() {
		return atkStone;
	}
	
	


}


