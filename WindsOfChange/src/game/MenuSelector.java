package game;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;

public class MenuSelector implements ActionListener {

	private boolean[] buttonPushed = new boolean[3];
	private boolean newGame;
	private boolean loadGame;
	private boolean exitGame;
				
			
	MenuSelector(JButton inBtn) {
		String[] btnTexts = {"New Game", "Load Game", "Exit Game"};
		outerloop:
		for(int i=0; i<3; i++) {
			if(inBtn.getText().equals(btnTexts[i])) {
				buttonPushed[i]=true;
				break outerloop;
			}//end if
		}//end for
		newGame = isNewGame(newGame);
		loadGame = isLoadGame(loadGame);
		exitGame = isExitGame(exitGame);
	}
	
	private boolean isNewGame(boolean answer) {
		if(buttonPushed[0]==true) {
			answer = true;
		}
		
		return answer;
	}
	
	private boolean isLoadGame(boolean answer) {
		if(buttonPushed[1]==true) {
			answer = true;
		}
		
		return answer;
	}
	
	private boolean isExitGame(boolean answer) {
		if(buttonPushed[2]==true) {
			answer = true;
		}
		
		return answer;
	}
	
	private String nameNull(String n) {
		if(n.equals("")) {
			n = "Nickodemus";
		}
		
		return n;
	}
	
	private void charTypePick(String name) {
		String charType;
		boolean wasAType = false;
		do {
		charType = JOptionPane.showInputDialog("Its Time To Remember What Class of Person You Were" + name + ":"
				+ "\n\n(A Guardian - "
				+ "Better with defense and low attack)\n (An Elite - Better with strength and low stealth)"
				+ "\n(An Assasin - Better with stealth and low defense) "
				+ "\n\nEnter type Guardian, Elite or Assasin below:").toUpperCase();
		wasAType = validateType(charType);
		}while(wasAType==false);
		MainWindsFrame.setChosenType(charType);
		
	}
	
	
	private boolean validateType(String charT) {
		if(charT.equals("GUARDIAN") || charT.equals("ELITE") || charT.equals("ASSASIN")) {
			return true;
		}else {
			JOptionPane.showMessageDialog(null, "Please enter one of three options as written!");
			return false;
		}
				
	}
		

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if(newGame==true) {
			MainWindsFrame.OMenu.setVisible(false);
			String disp = MainWindsFrame.texts.makeDialogText(0, 15);
			JOptionPane.showMessageDialog(null, disp);
			String disp2 = MainWindsFrame.texts.makeDialogText(15, MainWindsFrame.texts.getIntro().length);
			JOptionPane.showMessageDialog(null, disp2);
			String name = JOptionPane.showInputDialog("Do you remember your name?:");
			name = nameNull(name);
			MainWindsFrame.board.insertToDisplay("Welcome " + name + "!  -----------", 0);
			charTypePick(name);
			
			MainWindsFrame.ff.setVisible(true);
						
		}else if(loadGame==true) {
			
		}else if(exitGame==true) {
			System.exit(0);
		}
		
	}
	
}
