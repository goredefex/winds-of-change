package game;

public class Elite extends NewCharacter {

	private String className;
	
	//Specific char stats
	private int strE = 10;
	private int defE = 3;
	private int HPE = 250;
	private int stealthE = 1;
	private int attackE = 300;
		
		
	Elite(String nn) {
		super(nn);
		className = nn;
		this.addStr(strE);
		this.addDef(defE);
		this.addHP(HPE);
		this.addStealth(stealthE);
		this.addAtk(attackE);
		this.setCurrentGameHp(this.getHP());
		
	}
	
	
}
