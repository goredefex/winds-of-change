package game;

public class Assasin extends NewCharacter  {

	private String className;
	
	//Specific char stats
	private int strA = 7;
	private int defA = 2;
	private int HPA = 250;
	private int stealthA = 10;
	private int attackA = 200;
			
	
	Assasin(String nn) {
		super(nn);
		className = nn;
		this.addStr(strA);
		this.addDef(defA);
		this.addHP(HPA);
		this.addStealth(stealthA);
		this.addAtk(attackA);
		this.setCurrentGameHp(this.getHP());
		
	}
	
	
	
}
