package game;

import java.io.*;
import java.util.*;

import javax.swing.JOptionPane;

public class TextFiles {
	
	private Scanner scCounter = new Scanner(this.getClass().getResourceAsStream("/game/resources/intro.txt"));
	private Scanner sc = new Scanner(this.getClass().getResourceAsStream("/game/resources/intro.txt"));
	private String[] intro;
	private String[] introPt2;
	private int introLineCount = 0;
		
	TextFiles() {
		introLineCount = countIntroLines(introLineCount,scCounter);
		intro = new String[introLineCount];
		intro = introRead(intro, sc);
	}
		
	private String[] introRead(String[] i, Scanner s) {
		int counter = 0;
		while (s.hasNext() == true) {
			i[counter] = s.nextLine();
			counter++;
		}
		s.close();	
				
		return i;
	}
	
	private int countIntroLines(int count, Scanner s) {
		while (s.hasNext() == true) {
			count++;
			s.nextLine();
		}
		s.close();
		
		return count;
	}
	
	public String[] getIntro() {
		return intro;		
	}
	
	public String[] getIntro2() {
		return introPt2;		
	}
	
	public String[] getNumberOfIntro(int num) {
		String[] newIntro = new String[num];
		for(int i=0; i<num; i++) {
			newIntro[i] = intro[i];
		}
		
		return newIntro;
	}
	
	public String makeDialogText(int start, int number) {
		String regSize[] = getNumberOfIntro(number);
		String dialogSizedText = "";
		for(int i=start; i<number; i++) {
			dialogSizedText += regSize[i] + "\n";
		}
		
		return dialogSizedText;
	}
	
	
	
}
