package game;

import javax.imageio.*;
import javax.swing.*;

import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.net.URL;

public class GameBoardPiece extends JPanel {
	
	public static BufferedImage iconPiece;
	
	int pieceX = MainWindsFrame.roomNumber[1] * MainWindsFrame.board.getRoom().boundWidth+15;
	int pieceY = MainWindsFrame.roomNumber[0] * MainWindsFrame.board.getRoom().boundHeight;
	int pieceWidth = 40;
	int pieceHeight = 45;
	
	public GameBoardPiece() {
		try {
			iconPiece = ImageIO.read(GameBoardPiece.class.getResource("//game//resources//link.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void drawing() {
		repaint();
	}
			
	
	public void paint(Graphics g) {
		super.paint(g);
		
		g.drawImage(iconPiece, 10, 10, 40, 45, this);
			
	}
	
	
}
