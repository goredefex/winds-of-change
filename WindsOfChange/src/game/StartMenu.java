package game;

import javax.swing.*;
import java.awt.*;

public class StartMenu {

	private int menuWidth = 300;
	private int menuHeight = 180;
	
	private JFrame menu = new JFrame("The Winds of Change - Menu");
	private JLabel menuTitle = new JLabel("The Winds of Change");
			
	public StartMenu() {
		
		//Creation of menu frame
		menu = AdjustFrame(menu, menuWidth, menuHeight); 
		
		//Creation of menu title
		menuTitle = AdjustLabel(menuTitle);
				
		//New Game Button Creation
		JButton newButton = new JButton("New Game");
		newButton = setAndPlaceButton(newButton);
		MenuSelector newButtonPress = new MenuSelector(newButton);
		newButton.addActionListener(newButtonPress);
		
		//Load Game Button Creation
		JButton loadButton = new JButton("Load Game");
		loadButton = setAndPlaceButton(loadButton);
		MenuSelector loadButtonPress = new MenuSelector(loadButton);
		loadButton.addActionListener(loadButtonPress);
		
		//Load Game Button Creation
		JButton exitButton = new JButton("Exit Game");
		exitButton = setAndPlaceButton(exitButton);
		MenuSelector exitButtonPress = new MenuSelector(exitButton);
		exitButton.addActionListener(exitButtonPress);
		
		//Additions to frame
		menu.add(menuTitle);
		menu.add(newButton);
		menu.add(loadButton);
		menu.add(exitButton);
	}
	
	private JFrame AdjustFrame(JFrame ff, int width, int height) { //Make JFrame
		ff.setSize(width, height);
		ff.setLocationRelativeTo(null);
		ff.setVisible(true);
		ff.setResizable(false);
		ff.getContentPane().setLayout(null);
		ff.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
		return ff;
	}
	
	private JButton setAndPlaceButton(JButton inBtn) {
		int[][] buttonDetails = {{100, 50, 99, 23}, //New Game
								 {100, 83, 99, 23},  //Load Game
								 {100, 113, 99, 23}  //Exit Game
								 };
		String[] btnTexts = {"New Game", "Load Game", "Exit Game"};
		for(int i=0; i<3; i++) {
			if(inBtn.getText().equals(btnTexts[i])) {
				inBtn.setBounds(buttonDetails[i][0], buttonDetails[i][1], buttonDetails[i][2], buttonDetails[i][3]);
				break;
			}//end if
		}//end for
		
		return inBtn;
	}
	
	private JLabel AdjustLabel(JLabel title) {
		title.setBounds(90,  10, 120, 30);
		
		return title;
	}
	
	
	
	public JFrame getFrame() {
		return menu;
	}
	
	public void clearContents() {
		menu.getContentPane().removeAll();
	}
		
		
	
}
