
public class NewCharacter {

	String name;
	String choosenCharType;
	int str = 5;
	int def = 2;
	int HP = 500;
	int EXP = 0;
	int level = 1;
	
	//Overloaded constructor
	public NewCharacter(String inName, String charType, int inStr, int inDef, int inHP) {
		name = inName;
		choosenCharType = charType;
		str += inStr;
		def += inDef;
		HP += inHP;
		
	}
	
	
	//                               ********STR section*********
	
	//Start method to allow program to add to str
	public void addStr(int addStr) {
		str += addStr;
	}
	
	//Start method to completely change str
	public void changeStrValue(int newStr) {
		str = newStr;
	}
	
	
    //                               ********-HP section*********
	
	//Start method to allow program to add to HP
	public void addHP(int addHP) {
		HP += addHP;
	}
		
	//Start method to completely change HP
	public void changeHP(int newHP) {
		HP = newHP;
	}
	
	
	//							    ********-LVL section*********
	
	public void lvlUp() {
		level += 1;
	}
	
	public void addNumOfLvls(int addLvls) {
		level += addLvls;
	}
	
	public void changeLevel(int newLvl) {
		level = newLvl;
	}
	
	
	//						        ********-EXP section*********
	
	public void addEXP(int addEXP) {
		EXP += addEXP;
	}
	
	public void newEXP(int newEXP) {
		EXP = newEXP;
	}
	

} // End class

